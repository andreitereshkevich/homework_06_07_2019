package org.itstep.qa;

import org.testng.annotations.DataProvider;

public class InputPassword {
    @DataProvider(name = "inputIncorrectPasswords")
    public Object[][] inputDifferentPasswords() {
        return new Object[][]{
                {"123456"},
                {"123456aA12123456aA12123456aA12123456aA12"}
        };
    }

    @DataProvider(name = "inputEmptyPassword")
    public Object[][] inputEmptyPassword() {
        return new Object[][]{
                {""}

        };
    }
    @DataProvider(name = "wrongEmail")
    public Object[][] wrongEmail() {
        return new Object[][]{
                {"123456789012345678901234567890abc"},
                {"1"}

        };
    }

}
