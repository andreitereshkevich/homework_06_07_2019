package org.itstep.qa;
/**На форме регистрации rambler реализовать следующие тесты:
 1.	Необходимые тесты на валидацию пароля
 2.	Тест на проверку совпадения паролей
 3.	Необходимые тесты на валидацию имени почтового ящика
 */
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestRambler {
    private WebDriver driver;

    @BeforeClass
    public void createWebDriver() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver75.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @BeforeMethod
    public void openStartPage() {
        driver.get("https://id.rambler.ru/login-20/mail-registration?back=https%3A%2F%2Fwww.rambler.ru%2F%3Futm_source%3Dmail%26utm_content%3Dhead%26utm_medium%3Dlogo%26utm_campaign%3Dself_promo&rname=head&param=iframe&iframeOrigin=https%3A%2F%2Fwww.rambler.ru");
        }
    @Test(dataProviderClass = InputPassword.class,dataProvider = "inputIncorrectPasswords")
    public void incorrectPassword(String value){
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.id("newPassword")).sendKeys(value);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertTrue(driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/article/form/div[2]/section[1]/div/div/div[2]/div")).isDisplayed());
    }
    @Test(dataProviderClass = InputPassword.class,dataProvider = "inputEmptyPassword")
    public void emptyPassword(String value) {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.id("newPassword")).sendKeys(value);
        driver.findElement(By.id("confirmPassword")).click();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertTrue(driver.findElement(By.cssSelector(".rui-InputStatus-message")).isDisplayed());
    }
        @Test
        public void differentPasswords() {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            driver.findElement(By.id("newPassword")).sendKeys("123456aA12");
            driver.findElement(By.id("confirmPassword")).sendKeys("1");
            driver.findElement(By.id("login")).click();
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Assert.assertTrue(driver.findElement(By.cssSelector(".rui-InputStatus-message")).isDisplayed());

    }
    @Test
    public void samePasswords() {
        String strongPassword="123456aA12";
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.id("newPassword")).sendKeys(strongPassword);
        driver.findElement(By.id("confirmPassword")).sendKeys(strongPassword);
        driver.findElement(By.id("login")).sendKeys("a.teres");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertTrue(driver.findElement(By.cssSelector(".rui-InputStatus-message")).isDisplayed());

    }
    @Test(dataProviderClass = InputPassword.class,
            dataProvider = "wrongEmail")
    public void incorrectEmail(String value) {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.id("login")).sendKeys(value);
        driver.findElement(By.id("newPassword")).click();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertTrue(driver.findElement(By.cssSelector(".rui-InputStatus-message")).isDisplayed());
    }
    @Test()
    public void correctEmail() {
        String rightEmail="a.teres";
        String strongPassword="123456aA12";
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.id("login")).sendKeys(rightEmail);
        driver.findElement(By.id("newPassword")).sendKeys(strongPassword);
        driver.findElement(By.id("confirmPassword")).sendKeys(strongPassword);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertTrue(driver.findElement(By.cssSelector(".rui-InputStatus-message")).isDisplayed());
    }
    @AfterClass
    public void stop() {
        driver.manage().deleteAllCookies();
        driver.close();
    }
}
